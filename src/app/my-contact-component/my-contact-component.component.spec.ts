import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyContactComponentComponent } from './my-contact-component.component';

describe('MyContactComponentComponent', () => {
  let component: MyContactComponentComponent;
  let fixture: ComponentFixture<MyContactComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyContactComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyContactComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
