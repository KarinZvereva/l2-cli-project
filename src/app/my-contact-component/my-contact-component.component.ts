import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-contact-component',
  templateUrl: './my-contact-component.component.html',
  styleUrls: ['./my-contact-component.component.css']
})
export class MyContactComponentComponent implements OnInit {

  name : string = '';
  nickname : string = '';
  email : string = '';
  phone : string = '';
  dir : string = '';
  info : string = '';

  constructor() { }

  ngOnInit() {
  }

}
