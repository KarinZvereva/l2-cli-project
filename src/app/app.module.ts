import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { MyFirstModuleModule } from './my-first-module/my-first-module.module';
import { MyContactComponentComponent } from './my-contact-component/my-contact-component.component';

@NgModule({
  declarations: [
    AppComponent,
    MyContactComponentComponent
  ],
  imports: [
    BrowserModule,
    MyFirstModuleModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
